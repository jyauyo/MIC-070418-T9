package pe.edu.cjava.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import pe.edu.cjava.dto.Renta;
import pe.edu.cjava.json.util.JsonDateSerializer;

/**
 * @author jyauyo
 * 
 */
public class Rent {

	@JsonIgnore
	Renta renta = new Renta();

	public Rent(Renta renta) {
		this.renta = renta;
	}

	public int getId() {
		return renta.getIdRenta();
	}

	public void setId(int idRenta) {
		this.renta.setIdRenta(idRenta);
	}

	public int getOriginStationId() {
		return renta.getEstacionOrigen().getId();
	}

	public void setOriginStationId(int originStationId) {
		this.renta.getEstacionOrigen().setId(originStationId);
	}

	public int getBikeNumber() {
		return renta.getBike().getId();
	}

	public void setBikeNumber(int bikeNumber) {
		renta.getBike().setId(bikeNumber);
	}

	public int getDestinationStationId() {
		return renta.getEstacionDestino().getId();
	}

	public void setDestinationStationId(int destinationStationId) {
		this.renta.getEstacionDestino().setId(destinationStationId);
	}
	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getStartDate() {
		return renta.getFechaInicio();
	}

	public void setStartDate(Date startDate) {
		this.renta.setFechaInicio(startDate);
	}
	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getEndDate() {
		return renta.getFechaFin();
	}

	public void setEndDate(Date endDate) {
		this.renta.setFechaFin(endDate);
	}

	public boolean isActive() {
		return renta.isActivo();
	}

	public void setActive(boolean active) {
		this.renta.setActivo(active);
	}
}
