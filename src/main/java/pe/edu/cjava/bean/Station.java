package pe.edu.cjava.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import pe.edu.cjava.dto.Estacion;

/**
 * @author jyauyo
 * 
 */
public class Station {
	
	@JsonIgnore
	Estacion estacion = new Estacion();
	
	public Station(Estacion estacion){
		this.estacion = estacion;
	}
	
	int id;
	public int getId() {
		return this.estacion.getId();
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return this.estacion.getNombre();
	}
	public void setName(String name) {
		this.estacion.setNombre(name);
	}
	public int getLatitud() {
		return this.estacion.getLatitud();
	}
	public void setLatitud(int latitud) {
		this.estacion.setLatitud(latitud);;
	}
	public int getLongitud() {
		return this.estacion.getLongitud();
	}
	public void setLongitud(int longitud) {
		this.estacion.setLongitud(longitud);;
	}
	public int getAvalaibleBikes() {
		return this.estacion.getCantidadBikesDisponibles();
	}
	public void setAvalaibleBikes(int avalaibleBikes) {
		this.estacion.setCantidadBikesDisponibles(avalaibleBikes);
	}
}
