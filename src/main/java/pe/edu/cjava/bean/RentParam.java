package pe.edu.cjava.bean;

/**
 * @author jyauyo
 * 
 */
public class RentParam {
	
	int stationNumberFrom;
	int bikeNumberAvalaible;
	int stationNumberTo;
	
	public int getStationNumberFrom() {
		return stationNumberFrom;
	}
	public void setStationNumberFrom(int stationNumberFrom) {
		this.stationNumberFrom = stationNumberFrom;
	}
	public int getBikeNumberAvalaible() {
		return bikeNumberAvalaible;
	}
	public void setBikeNumberAvalaible(int bikeNumberAvalaible) {
		this.bikeNumberAvalaible = bikeNumberAvalaible;
	}
	public int getStationNumberTo() {
		return stationNumberTo;
	}
	public void setStationNumberTo(int stationNumberTo) {
		this.stationNumberTo = stationNumberTo;
	}


}
