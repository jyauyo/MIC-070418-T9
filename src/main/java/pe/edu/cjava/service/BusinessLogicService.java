package pe.edu.cjava.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import pe.edu.cjava.bean.Rent;
import pe.edu.cjava.bean.RentParam;
import pe.edu.cjava.bean.Station;
import pe.edu.cjava.dropwizard.config.DropwizardConfiguration;
import pe.edu.cjava.dto.Bike;
import pe.edu.cjava.dto.Estacion;
import pe.edu.cjava.dto.Renta;
import pe.edu.cjava.dto.Usuario;
import pe.edu.cjava.dto.UsuarioRenta;
import pe.edu.cjava.respuesta.ResponseBean;
import pe.edu.cjava.respuesta.Respuesta;

/**
 * @author jyauyo
 * 
 */
public class BusinessLogicService {

	List<Bike> listaBikes = new ArrayList<Bike>();
	Map<Integer, Bike> mpBikes = new HashMap<Integer, Bike>();

	Map<Integer, Estacion> mpEstaciones = new HashMap<Integer, Estacion>();
	List<Estacion> listaEstaciones = new ArrayList<Estacion>();

	Map<Integer, Renta> mpRentas = new HashMap<Integer, Renta>();
	List<UsuarioRenta> usuarioRentas = new ArrayList<UsuarioRenta>();

	List<Usuario> listUsuarios = new ArrayList<Usuario>();

	public BusinessLogicService(DropwizardConfiguration config) {
		armarDataFromYAML(config);
	}

	/**
	 * @author jyauyo
	 * 
	 * Obtiene las estaciones
	 * 
	 */
	public ResponseBean getEstaciones(String auth) {

		ResponseBean rpta = this.autorizacion(auth);
		List<Station> lstStation = new ArrayList<Station>();
		listaEstaciones.forEach(s -> {
			lstStation.add(new Station(s));
		});
		if (rpta.getTipoRespuesta().equals(Status.OK)) {
			rpta.setTipoRespuesta(Status.OK);
			rpta.setJsonObject(lstStation);
		}
		return rpta;
	}

	/**
	 * @author jyauyo
	 * 
	 * Obtiene el historial de Rentas del usuario
	 * 
	 */
	public ResponseBean getRentas(String auth) {

		ResponseBean rpta = this.autorizacion(auth);

		if (rpta.getTipoRespuesta().equals(Status.OK)) {

			List<Renta> lstRentas = getRentasUsuario((Usuario) rpta.getJsonObject(), false);
			List<Rent> lstHistoryRents = new ArrayList<Rent>();
			lstRentas.forEach(r -> {
				lstHistoryRents.add(new Rent(r));
			});

			rpta.setJsonObject(lstHistoryRents);
		}

		return rpta;
	}

	/**
	 * @author jyauyo
	 * 
	 * Graba la Renta
	 */
	public ResponseBean generarRenta(String auth, RentParam rentParam) {

		ResponseBean rpta = this.autorizacion(auth);

		if (rpta.getTipoRespuesta().equals(Status.OK)) {

			List<Renta> rentas = getRentasUsuario(((Usuario) rpta.getJsonObject()), true);
			if (!rentas.isEmpty()) {
				rpta.setTipoRespuesta(Status.CONFLICT);
				rpta.setJsonObject(new Respuesta(Status.CONFLICT.getStatusCode(),
						"Existe una renta activa con nro '" + rentas.get(0).getIdRenta() + "' en este momento"));
				return rpta;
			}

			if (!mpEstaciones.containsKey(rentParam.getStationNumberFrom())) {
				ResponseBean rsp = new ResponseBean(Status.NOT_FOUND, listaEstaciones,
						"No existe Estacion origen nro: '" + rentParam.getStationNumberFrom()
								+ "' a Rentar, Ver las Lista de Estaciones disponibles");

				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(rsp);

				return rpta;
			}
			if (!mpEstaciones.containsKey(rentParam.getStationNumberTo())) {
				ResponseBean rsp = new ResponseBean(Status.NOT_FOUND, listaEstaciones,
						"No existe Estacion destino nro: '" + rentParam.getStationNumberTo()
								+ "' a Rentar, Ver las Lista de Estaciones disponibles");

				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(rsp);
				return rpta;

			}

			if (rentParam.getStationNumberFrom() == rentParam.getStationNumberTo()) {

				rpta.setTipoRespuesta(Status.NOT_ACCEPTABLE);
				rpta.setJsonObject(
						new Respuesta(Status.NOT_ACCEPTABLE.getStatusCode(), "Estacion Origen y Destino Iguales"));

				return rpta;
			}

			if (!mpBikes.containsKey(rentParam.getBikeNumberAvalaible())) {
				ResponseBean rsp = new ResponseBean(Status.NOT_FOUND, listaBikes, "No existe Bike nro: '"
						+ rentParam.getBikeNumberAvalaible() + "' a Rentar, Ver las Lista de Bikes disponibles");

				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(rsp);

				return rpta;
			}

			Renta rentaNueva = new Renta();
			rentaNueva.setIdRenta((int) (Math.random() * 100));
			rentaNueva.setBike(mpBikes.get(rentParam.getBikeNumberAvalaible()));
			rentaNueva.setEstacionOrigen(mpEstaciones.get(rentParam.getStationNumberFrom()));
			rentaNueva.setEstacionDestino(mpEstaciones.get(rentParam.getStationNumberTo()));
			rentaNueva.setActivo(true);

			Date fechaInicio = new Date();
			rentaNueva.setFechaInicio(fechaInicio);

			UsuarioRenta ur = new UsuarioRenta();
			ur.setRenta(rentaNueva);
			ur.setUsuario(((Usuario) rpta.getJsonObject()));

			usuarioRentas.add(ur);
			mpRentas.put(rentaNueva.getIdRenta(), rentaNueva);

			rpta.setTipoRespuesta(Status.CREATED);
			rpta.setJsonObject(new Rent(rentaNueva));

		}
		return rpta;
	}

	/**
	 * @author jyauyo
	 * 
	 * Actualiza el destino de la Renta Activa
	 * 
	 */
	public ResponseBean actualizarRenta(String auth, int destinationStationId) {

		ResponseBean rpta = this.autorizacion(auth);

		if (rpta.getTipoRespuesta().equals(Status.OK)) {
			List<Renta> rentas = getRentasUsuario(((Usuario) rpta.getJsonObject()), true);

			if (rentas.isEmpty()) {
				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(
						new Respuesta(Status.NOT_FOUND.getStatusCode(), "No existe Renta Activa a Actualizar"));

				return rpta;
			}

			System.out.println("Renta activa del Usuario " + ((Usuario) rpta.getJsonObject()).getLogin());
			Renta r = rentas.get(0);

			if (mpEstaciones.containsKey(destinationStationId)) {
				System.out.println("Se Actualizara la Renta id: " + r.getIdRenta() + " con destino de "
						+ r.getEstacionDestino().getUbicacion() + " a "
						+ mpEstaciones.get(destinationStationId).getUbicacion());
				r.setEstacionDestino(mpEstaciones.get(destinationStationId));

				for (UsuarioRenta ur : usuarioRentas) {
					if (r.getIdRenta() == ur.getRenta().getIdRenta()) {
						ur.setRenta(r);
						mpRentas.put(r.getIdRenta(), r);
						break;
					}
				}

				rpta.setTipoRespuesta(Status.ACCEPTED);
				rpta.setJsonObject(new Rent(r));

				return rpta;

			} else {
				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(new Respuesta(Status.NOT_FOUND.getStatusCode(), "No existe Destino a actualizar"));
				return rpta;
			}
		}

		return rpta;
	}

	/**
	 * @author jyauyo
	 * 
	 * Cancela la Renta Activa
	 * 
	 */
	public ResponseBean cancelarRenta(String auth) {

		ResponseBean rpta = this.autorizacion(auth);

		if (rpta.getTipoRespuesta().equals(Status.OK)) {
			List<Renta> rentas = getRentasUsuario(((Usuario) rpta.getJsonObject()), true);
			Map<Integer, Renta> mpRentase = new HashMap<Integer, Renta>();
			rentas.forEach(r -> {
				mpRentase.put(r.getIdRenta(), r);
			});

			if (rentas.isEmpty()) {
				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(
						new Respuesta(Status.NOT_FOUND.getStatusCode(), "No existe Renta Activa a Cancelar"));

				return rpta;
			}

			if (mpRentase.containsKey(rentas.get(0).getIdRenta())) {
				System.out.println("Se cancelara la renta " + rentas.get(0).getIdRenta());
				for (UsuarioRenta ur : usuarioRentas) {
					if (rentas.get(0).getIdRenta() == ur.getRenta().getIdRenta()) {
						usuarioRentas.remove(ur);
						mpRentas.remove(rentas.get(0).getIdRenta());
						break;
					}
				}
			} else {
				rpta.setTipoRespuesta(Status.NOT_FOUND);
				rpta.setJsonObject(new Respuesta(Status.NOT_FOUND.getStatusCode(), "No existe Renta Cancelar"));
				return rpta;
			}
			rpta.setTipoRespuesta(Status.OK);
			rpta.setJsonObject(new Respuesta(Status.OK.getStatusCode(),
					"Renta activa nro " + rentas.get(0).getIdRenta() + " cancelada"));

			return rpta;
		}

		return rpta;
	}

	/**
	 * @author jyauyo
	 * @param Usuario usuario
	 * @param boolean onlyActivo
	 * 
	 * Obtiene las rentas del usuario activos y no activos
	 * 
	 */
	private List<Renta> getRentasUsuario(Usuario usuario, boolean onlyActivo) {

		List<Renta> listaRentasByUsuario = new ArrayList<>();

		for (UsuarioRenta ur : usuarioRentas) {
			if (ur.getUsuario().getLogin().equals(usuario.getLogin())) {

				if (mpRentas.containsKey(ur.getRenta().getIdRenta())) {
					if (onlyActivo) {
						if (mpRentas.get(ur.getRenta().getIdRenta()).isActivo())
							listaRentasByUsuario.add(mpRentas.get(ur.getRenta().getIdRenta()));
					} else {
						listaRentasByUsuario.add(mpRentas.get(ur.getRenta().getIdRenta()));
					}
				}
			}
		}

		return listaRentasByUsuario;
	}

	
	/**
	 * @author jyauyo
	 * 
	 * Arma la data hardcode desde el config.yaml
	 */
	private Map<Integer, Renta> armarDataFromYAML(DropwizardConfiguration config) {
		System.out.println("Se carga la data HardCode");

		listUsuarios = config.getUsuarios();

		usuarioRentas = config.getUsuarioRentas();

		listaBikes = config.getBikes();
		listaBikes.forEach(b -> {
			mpBikes.put(b.getId(), b);
		});

		listaEstaciones = config.getEstaciones();
		listaEstaciones.forEach(e -> {
			mpEstaciones.put(e.getId(), e);
		});

		List<Renta> listaRentas = config.getRentas();

		for (Renta r : listaRentas) {
			r.setBike(mpBikes.get(r.getBike().getId()));
			r.setEstacionDestino(mpEstaciones.get(r.getEstacionDestino().getId()));
			r.setEstacionOrigen(mpEstaciones.get(r.getEstacionOrigen().getId()));
		}

		listaRentas.forEach(r -> {
			mpRentas.put(r.getIdRenta(), r);
		});

		return mpRentas;
	}
	
	/**
	 * @author jyauyo
	 * 
	 * Valida el usuario
	 * 
	 */
	private ResponseBean autorizacion(String auth) {
		ResponseBean response = null;

		response = new ResponseBean(Status.OK, listUsuarios.get(0));
		return response;

		/*
		 * Usuario usuarioLogeado = null; boolean isAutorizado = false; String[]
		 * data = auth.split("\\s+");
		 * 
		 * String encripta = data[1];
		 * 
		 * byte[] bytes = null; try { bytes = new
		 * BASE64Decoder().decodeBuffer(encripta); } catch (Exception e) {
		 * e.printStackTrace(); } String decode = new String(bytes);
		 * 
		 * String[] credenciales = decode.split(":");
		 * 
		 * for (UsuarioLogin usuario : listUsuarios) { if
		 * (usuario.getLogin().equals(credenciales[0]) &&
		 * usuario.getPwd().equals(credenciales[1])) { isAutorizado = true;
		 * usuarioLogeado = usuario;
		 * 
		 * break; } }
		 * 
		 * if (!isAutorizado) { response = new ResponseBean(Status.UNAUTHORIZED,
		 * new Respuesta(Status.UNAUTHORIZED.getStatusCode(),
		 * "Usuario No Autorizado"));
		 * 
		 * } else { response = new ResponseBean(Status.OK, usuarioLogeado); }
		 * return response;
		 */
	}

}
