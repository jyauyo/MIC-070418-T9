package pe.edu.cjava.dto;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import pe.edu.cjava.json.util.JsonDateSerializer;

/**
 * @author jyauyo
 * 
 */
public class Renta {

	int idRenta;
	Estacion estacionOrigen;	
	Estacion estacionDestino;
	Bike bike;
	@JsonSerialize(using = JsonDateSerializer.class)	
	Date fechaInicio;
	@JsonSerialize(using = JsonDateSerializer.class)	
	Date fechaFin;	
	boolean isActivo;

	public int getIdRenta() {
		return idRenta;
	}

	public void setIdRenta(int idRenta) {
		this.idRenta = idRenta;
	}

	public Estacion getEstacionOrigen() {
		if(estacionOrigen==null)
			estacionOrigen = new Estacion();
		
		return estacionOrigen;
	}

	public void setEstacionOrigen(Estacion estacionOrigen) {
		this.estacionOrigen = estacionOrigen;
	}

	public Bike getBike() {
		if(bike==null)
			bike = new Bike();
		return bike;
	}

	public void setBike(Bike bike) {		
		this.bike = bike;
	}

	public Estacion getEstacionDestino() {
		if(estacionDestino==null)
			estacionDestino = new Estacion();
		return estacionDestino;
	}

	public void setEstacionDestino(Estacion estacionDestino) {
		this.estacionDestino = estacionDestino;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public boolean isActivo() {
		return isActivo;
	}

	public void setActivo(boolean isActivo) {
		this.isActivo = isActivo;
	}

}
