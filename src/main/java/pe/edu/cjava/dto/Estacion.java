package pe.edu.cjava.dto;

/**
 * @author jyauyo
 * 
 */
public class Estacion {
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public int getCantidadBikesDisponibles() {
		return cantidadBikesDisponibles;
	}
	public void setCantidadBikesDisponibles(int cantidadBikesDisponibles) {
		this.cantidadBikesDisponibles = cantidadBikesDisponibles;
	}
	
	int id;
	String nombre;
	String ubicacion;
	int cantidadBikesDisponibles;
	int latitud;
	public int getLatitud() {
		return latitud;
	}
	public void setLatitud(int latitud) {
		this.latitud = latitud;
	}
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	int longitud;

}
