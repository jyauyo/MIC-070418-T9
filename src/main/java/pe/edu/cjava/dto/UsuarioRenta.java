package pe.edu.cjava.dto;

/**
 * @author jyauyo
 * 
 */
public class UsuarioRenta {
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Renta getRenta() {
		return renta;
	}
	public void setRenta(Renta renta) {
		this.renta = renta;
	}
	Usuario usuario;
	Renta renta;
}
