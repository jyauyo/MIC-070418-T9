package pe.edu.cjava.dto;

/**
 * @author jyauyo
 * 
 */
public class Bike {
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	int id;
	String color;

}
