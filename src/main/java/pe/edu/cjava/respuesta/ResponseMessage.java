package pe.edu.cjava.respuesta;

abstract class ResponseMessage {
	
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
