package pe.edu.cjava.respuesta;

import javax.ws.rs.core.Response.Status;

/**
 * @author jyauyo
 * 
 */
public class ResponseBean extends ResponseMessage {
	
	public ResponseBean(Status tipoRespuesta, Object jsonObject) {
		this.tipoRespuesta = tipoRespuesta;
		this.jsonObject = jsonObject;
	}
	
	public ResponseBean(Status tipoRespuesta, Object jsonObject, String message) {
		
		this.tipoRespuesta = tipoRespuesta;
		this.jsonObject = jsonObject;
		this.message = message;
	}
	private Status tipoRespuesta;
	
	private Object jsonObject;
	
	public Object getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(Object jsonObject) {
		this.jsonObject = jsonObject;
	}
	public Status getTipoRespuesta() {
		return tipoRespuesta;
	}
	public void setTipoRespuesta(Status tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
}
