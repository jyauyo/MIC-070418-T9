package pe.edu.cjava.respuesta;

/**
 * @author jyauyo
 * 
 */
public class Respuesta {
	
	public Respuesta(int tipoRespuesta, String mensaje) {
		super();
		this.tipoRespuesta = tipoRespuesta;
		this.mensaje = mensaje;
	}
	private int tipoRespuesta;
	private String mensaje;	
	public int getTipoRespuesta() {
		return tipoRespuesta;
	}
	public void setTipoRespuesta(int tipoRespuesta) {
		this.tipoRespuesta = tipoRespuesta;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
