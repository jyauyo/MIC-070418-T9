package pe.edu.cjava.dropwizard.config;

import io.dropwizard.Configuration;
import pe.edu.cjava.dto.Bike;
import pe.edu.cjava.dto.Estacion;
import pe.edu.cjava.dto.Renta;
import pe.edu.cjava.dto.Usuario;
import pe.edu.cjava.dto.UsuarioRenta;

import java.util.List;

/**
 * @author jyauyo
 * 
 * */
public class DropwizardConfiguration extends Configuration {
    	
	
	private List<Usuario> usuarios;

	private List<Estacion> estaciones;
	private List<Bike> bikes;
	private List<Renta> rentas;
	private List<UsuarioRenta> usuarioRentas;
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public List<UsuarioRenta> getUsuarioRentas() {
		return usuarioRentas;
	}

	public void setUsuarioRentas(List<UsuarioRenta> usuarioRentas) {
		this.usuarioRentas = usuarioRentas;
	}

	public List<Renta> getRentas() {
		return rentas;
	}

	public void setRentas(List<Renta> rentas) {
		this.rentas = rentas;
	}

	public List<Bike> getBikes() {
		return bikes;
	}

	public void setBikes(List<Bike> bikes) {
		this.bikes = bikes;
	}

	public List<Estacion> getEstaciones() {
		return estaciones;
	}

	public void setEstaciones(List<Estacion> estaciones) {
		this.estaciones = estaciones;
	}
}
