package pe.edu.cjava.dropwizard.ws.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;

import io.dropwizard.jersey.PATCH;
import pe.edu.cjava.bean.RentParam;
import pe.edu.cjava.dropwizard.config.DropwizardConfiguration;
import pe.edu.cjava.respuesta.ResponseBean;
import pe.edu.cjava.service.BusinessLogicService;

/**
 * @author jyauyo
 * 
 */
@Path("/v1")
public class RestController {

	BusinessLogicService serviceRenta;

	public RestController(DropwizardConfiguration config) {
		serviceRenta = new BusinessLogicService(config);
	}

	@GET
	@Timed
	@Path("/stations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEstaciones(@HeaderParam("authorization") String auth) {
		System.out.println("@GET -> /estaciones");
		ResponseBean rpta = serviceRenta.getEstaciones(auth);
		return Response.status(rpta.getTipoRespuesta()).entity(rpta.getJsonObject()).build();
	}

	@GET
	@Timed
	@Path("/rents")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRentas(@HeaderParam("authorization") String auth) {

		System.out.println("@GET -> /lstHistoryRents");
		ResponseBean rpta = serviceRenta.getRentas(auth);
		return Response.status(rpta.getTipoRespuesta()).entity(rpta.getJsonObject()).build();
	}

	@POST
	@Timed
	@Path("/rents")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response generarRenta(@HeaderParam("authorization") String auth, RentParam rentParam) {

		System.out.println("@POST -> /rentas/{idOrigenDestination}/{idDestination}/{idBike}");

		ResponseBean rpta = serviceRenta.generarRenta(auth, rentParam);
		return Response.status(rpta.getTipoRespuesta()).entity(rpta.getJsonObject()).build();
	}

	@PATCH
	@Timed
	@Path("/rents/{destinationStationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response actualizarRenta(@HeaderParam("authorization") String auth,
			@PathParam("destinationStationId") int destinationStationId) {

		System.out.println("PATCH -> rentas/{destinationStationId}");
		ResponseBean rpta = serviceRenta.actualizarRenta(auth, destinationStationId);
		return Response.status(rpta.getTipoRespuesta()).entity(rpta.getJsonObject()).build();
	}

	@DELETE
	@Timed
	@Path("/rents")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelarRenta(@HeaderParam("authorization") String auth) {

		System.out.println("DELETE -> rentas/{idRenta}");
		ResponseBean rpta = serviceRenta.cancelarRenta(auth);
		return Response.status(rpta.getTipoRespuesta()).entity(rpta.getJsonObject()).build();
	}

}
