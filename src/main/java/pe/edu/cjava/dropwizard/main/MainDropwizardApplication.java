package pe.edu.cjava.dropwizard.main;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import pe.edu.cjava.dropwizard.config.DropwizardConfiguration;
import pe.edu.cjava.dropwizard.config.WeldBundle;
import pe.edu.cjava.dropwizard.ws.rest.RestController;

import com.codahale.metrics.health.HealthCheck;

/**
 * @author jyauyo
 * 
 */
public class MainDropwizardApplication extends Application<DropwizardConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MainDropwizardApplication().run(args);
        //new MainDropwizardApplication().run("server", resourceFilePath("src/main/resources/config.yaml"));
    }

    @Override
    public void initialize(final Bootstrap<DropwizardConfiguration> bootstrap) {
    	/*bootstrap.setConfigurationSourceProvider(
    			new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
    					new EnvironmentVariableSubstitutor(false)));*/
    	
    	bootstrap.addBundle(new WeldBundle());
    }

    @Override
    public void run(final DropwizardConfiguration config,
                    final Environment environment) {
    	
    	environment.servlets().addServletListeners(new org.jboss.weld.environment.servlet.Listener());
    	
    	environment.healthChecks().register("dummy", new HealthCheck() {
			@Override
			protected Result check() throws Exception {

				return Result.healthy("dummy");
			}
		});
    	
    	environment.jersey().register(new RestController(config));
    	//environment.jersey().register(RestController.class);    	
    }

}
